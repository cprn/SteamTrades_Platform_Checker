// ==UserScript==
// @name        SteamTrades Linux Checker
// @author      Cyprian Guerra
// @namespace   https://github.com/cprn
// @description Checks the Linux build availability for "I have..." titles on Steam. Very inaccurate but still helpful.
// @include     *://www.steamtrades.com/trade/*
// @version     0.1
// @grant       GM_xmlhttpRequest
// @connect     steampowered.com
// ==/UserScript==

var STEAM_SEARCH = 'http://store.steampowered.com/search/?term=',
    PENGUIN = '🐧',
    PAUSE = 4000;

var a = document.createElement('a'),
    ratio = document.createElement('span'),
    bar = document.createElement('span'),
    progress = document.createElement('span'),
    have = document.getElementsByClassName('have markdown')[0],
    list = document.createElement('ul'),
    checkedLinux = 0,
    areadyRun = false;

var schedule = function (ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};

var getElementByXpath = function (dom, path) {
  return document.evaluate(path, dom, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

var checkLinux = function (line, havesLinux) {
    url = STEAM_SEARCH + encodeURIComponent(line);
    GM_xmlhttpRequest({
        "method": "GET",
        "url": url,
        "onload": function(response) {
            var tmp = document.createElement('div');
            tmp.innerHTML = response.response;
            xpath = '//span[@class="title" and translate(text(),"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz")="' + line.toLowerCase() + '"]/..//span[@class="platform_img linux"]'
            if (Boolean(getElementByXpath(tmp, xpath))) {
                checkedLinux++;
                var li = document.createElement('li'),
                    a = document.createElement('a');
                a.href = url;
                a.target = 'blank';
                a.textContent = PENGUIN + ' ' + line;
                li.append(a);
                list.appendChild(li);
            }
        }
    });
};

document.getElementsByClassName('trade_heading')[0].append(
    a,
    ratio,
    bar,
    progress
);

have.insertBefore(list, have.firstChild);

bar.title = 'progress';
bar.style.marginLeft = '.5em';

progress.title = 'progress [%]'
progress.style.marginLeft = '.5em';

ratio.title = 'Linux to non-Linux ratio of what has been scanned so far';
ratio.style.marginLeft = '.5em';

a.title = 'click to search for Linux builds';
a.textContent = PENGUIN;
a.href = '#';
a.style.marginLeft = '2em';
a.addEventListener('click', function () {
    if (!areadyRun) {
        areadyRun = true;
        var haves = have.textContent.split("\n");
        haves.forEach(async function (line, i) {
            await schedule(PAUSE * i);
            checkLinux(line);
            i++;
            ratio.textContent = Math.round(checkedLinux / i * 100) / 100;
            bar.textContent = i + '/' + haves.length;
            progress.textContent = '[' + Math.round(i / haves.length * 100) + '%]';
        });
    }
});
